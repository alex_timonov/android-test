package alextimonov.api;

import alextimonov.api.entities.CreateOrganizationRequest;
import alextimonov.api.entities.CreateOrganizationResponse;
import alextimonov.api.entities.ImagesResponse;
import alextimonov.api.entities.LoginResponse;
import alextimonov.api.entities.SignUpRequest;
import alextimonov.api.entities.SignUpResponse;
import alextimonov.api.exceptions.ApiException;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

public interface RestApi {

    @POST("/user")
    SignUpResponse signUp(@Body SignUpRequest signUpRequest) throws ApiException;

    @POST("/login")
    LoginResponse logIn(@Body SignUpRequest signUpRequest) throws ApiException;

    @POST("/organization")
    CreateOrganizationResponse createOrganization(@Body CreateOrganizationRequest createOrganizationRequest) throws ApiException;

    @GET("/reader/bingImages/&search=Star+Wars&count=2")
    ImagesResponse loadImages() throws ApiException;

}