package alextimonov.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import alextimonov.TestApplication;
import alextimonov.api.entities.CreateOrganizationRequest;
import alextimonov.api.entities.CreateOrganizationResponse;
import alextimonov.api.entities.ImagesResponse;
import alextimonov.api.entities.LoginResponse;
import alextimonov.api.entities.SignUpRequest;
import alextimonov.api.entities.SignUpResponse;
import alextimonov.api.exceptions.ApiException;
import alextimonov.api.exceptions.CustomErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class Api {

    private RestApi restApi;
    private String apiUrl;
    private RequestInterceptor requestInterceptor;
    private TestApplication application;


    public String getApiBaseUrl() {
        return "http://release.linkboard.com";
    }

    public Api(TestApplication application) {
        this.application = application;
        initRequestInterceptor();
        apiUrl = getApiBaseUrl();
        restApi = initRestAdapter();
    }

    private void initRequestInterceptor() {
        requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                if (!application.getPreferences().username().getOr("").isEmpty()) {
                    request.addHeader("Username", application.getPreferences().username().get());
                    request.addHeader("Password", application.getPreferences().password().get());
                }
            }
        };
    }


    private RestApi initRestAdapter() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(60, TimeUnit.SECONDS);
        Gson gson = new GsonBuilder().create();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(apiUrl)
                .setClient(new OkClient(okHttpClient))
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(requestInterceptor)
                .setErrorHandler(new CustomErrorHandler(application))
                .build();
        return restAdapter.create(RestApi.class);
    }

    public SignUpResponse signUp(SignUpRequest signUpRequest) throws ApiException {
        return restApi.signUp(signUpRequest);
    }

    public LoginResponse logIn(SignUpRequest signUpRequest) throws ApiException {
        return restApi.logIn(signUpRequest);
    }

    public CreateOrganizationResponse createOrganization(CreateOrganizationRequest createOrganizationRequest) throws ApiException {
        return restApi.createOrganization(createOrganizationRequest);
    }

    public ImagesResponse loadImages() throws ApiException {
        return restApi.loadImages();
    }
}
