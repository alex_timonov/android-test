package alextimonov.api.exceptions;

import java.util.ArrayList;

public class ApiException extends Exception {

    private final ApiExceptionDetails details;

    public ApiException() {
        this("Unknown exception", null, null);
    }

    public ApiException(String detailMessage) {
        this(detailMessage, null, null);
    }

    public ApiException(String detailMessage, Throwable throwable) {
        this(detailMessage, null, throwable);
    }

    private ApiException(String message, ApiExceptionDetails details,
                         Throwable throwable) {
        super(message, throwable);

        if (details == null)
            details = new ApiExceptionDetails(new ArrayList<String>());

        this.details = details;
    }

    public ApiExceptionDetails getDetails() {
        return details;
    }

}
