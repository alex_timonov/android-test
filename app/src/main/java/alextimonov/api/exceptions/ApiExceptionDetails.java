package alextimonov.api.exceptions;

import java.util.List;


public class ApiExceptionDetails {

    private final List<String> errors;

    public ApiExceptionDetails(List<String> errors) {
        this.errors = errors;
    }

    public boolean contains(String str) {
        return errors.contains(str);
    }

}

