package alextimonov.api.exceptions;

import android.content.Context;

import alextimonov.R;
import retrofit.ErrorHandler;
import retrofit.RetrofitError;


public class CustomErrorHandler implements ErrorHandler {

    Context context;

    @Override
    public Throwable handleError(RetrofitError cause) {
        return genApiException(cause);
    }

    public CustomErrorHandler(Context context) {
        this.context = context;

    }

    private ApiException genApiException(RetrofitError cause) {

        RetrofitError.Kind errorKind = cause.getKind();
        if (errorKind == RetrofitError.Kind.NETWORK) {
            return new ApiException(context.getString(R.string.no_internet_connection));
        }
        if (errorKind == RetrofitError.Kind.HTTP) {

            String apiUrl = cause.getUrl();
            int statusCode = cause.getResponse().getStatus();
            if (statusCode == 401) {
                return new ApiException("401");
            }
        }


        return new ApiException(cause.getMessage(), cause);
    }
}
