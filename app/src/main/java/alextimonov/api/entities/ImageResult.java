package alextimonov.api.entities;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageResult implements Serializable {
    @SerializedName("__metadata")
    public ImageInfo metadata;
    public String MediaUrl;
}
