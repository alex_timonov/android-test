package alextimonov.api.entities;


import java.io.Serializable;

public class LoginResponse implements Serializable {
    public boolean status;
    public String message;
    public LoginResponseData data;
}
