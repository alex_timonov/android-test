package alextimonov.api.entities;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SignUpRequest implements Serializable {
    public String username;
    public String email;
    public String password;
    @SerializedName("organization_id")
    public int organizationId;
    @SerializedName("role_id")
    public int roleId;
}
