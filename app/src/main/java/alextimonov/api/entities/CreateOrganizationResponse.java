package alextimonov.api.entities;


import java.io.Serializable;

public class CreateOrganizationResponse implements Serializable {
    public boolean status;
    public String message;
}
