package alextimonov.api.entities;


import java.io.Serializable;

public class ImagesResponse implements Serializable {
    public boolean status;
    public String message;
    public ImagesResponseData data;
}
