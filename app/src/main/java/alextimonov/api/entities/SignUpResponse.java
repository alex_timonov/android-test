package alextimonov.api.entities;


import java.io.Serializable;

public class SignUpResponse implements Serializable {
    public boolean status;
    public String message;
}
