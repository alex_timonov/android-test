package alextimonov.api.entities;


import java.io.Serializable;
import java.util.ArrayList;

public class ImagesResponseData implements Serializable {
    public ArrayList<ImageResult> results;
}
