package alextimonov.api.entities;


import java.io.Serializable;

public class CreateOrganizationRequest implements Serializable {
    public String name;
    public String email;
}
