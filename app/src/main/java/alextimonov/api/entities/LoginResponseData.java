package alextimonov.api.entities;


import java.io.Serializable;

public class LoginResponseData implements Serializable {
    public long id;
    public String username;
    public String email;


//                "first_name": "Kaley",
//                "last_name": "Windler",
//                "dob": 812537498,
//                "desk_id": null,
//                "organization_id": 1,
//                "role_id": 1,
//                "created_at": "2015-10-29 20:01:56",
//                "updated_at": "2015-10-29 20:01:56",
//                "role_name": "Super Admin",
//                "organization_name": "Kertzmann, Schroeder and Mayer",
//                "organization_url": "http://Murray.biz/consequatur-ut-aliquid-est-placeat-illo-omnis-qui",
//                "organizations_email": "Gillian60@yahoo.com",
//                "organization_detail": "velit",
//                "organization_employees": 9,
//                "organization_start_link_token": "NOBIS",
//                "organization_copy_title": null,
//                "organization_copy_text": null

}
