package alextimonov;


import java.io.IOException;
import java.net.CookieManager;
import java.net.URI;
import java.util.List;
import java.util.Map;

import alextimonov.properties.UserPreferences_;

public class MyCookieManager extends CookieManager {

    UserPreferences_ preferences;

    public MyCookieManager(UserPreferences_ preferences) {
        this.preferences = preferences;
    }


    @Override
    public void put(URI uri, Map<String, List<String>> stringListMap) throws IOException {
        super.put(uri, stringListMap);
        if (stringListMap != null && stringListMap.get("user-token") != null)
            for (String string : stringListMap.get("user-token")) {
                preferences.token().put(string);
            }
    }
}