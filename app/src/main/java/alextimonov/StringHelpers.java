package alextimonov;

import android.util.Patterns;

public class StringHelpers {

    public static void checkEmail(String email, String errorMessage) {
        isNullOrEmptyString(email, errorMessage);
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            throw new IllegalArgumentException(errorMessage);
    }

    public static void isNullOrEmptyString(String str, String errorMessage) {
        if (!(str != null && !str.isEmpty()))
            throw new IllegalArgumentException(errorMessage);

    }

}
