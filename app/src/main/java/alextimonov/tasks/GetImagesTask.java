package alextimonov.tasks;

import com.telly.groundy.Groundy;
import com.telly.groundy.TaskResult;

import alextimonov.api.entities.ImagesResponse;
import alextimonov.api.exceptions.ApiException;


public class GetImagesTask extends GroundyTaskBase {

    public static final String RESPONSE = "response";

    @Override
    protected TaskResult doInBackground() {
        try {
            ImagesResponse imagesResponse = getApi().loadImages();
            return succeeded().add(RESPONSE, imagesResponse);
        } catch (ApiException e) {
            return failed(e);
        }
    }

    public static Groundy create() {
        return Groundy.create(GetImagesTask.class);
    }
}
