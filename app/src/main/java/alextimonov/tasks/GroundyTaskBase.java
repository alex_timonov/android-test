package alextimonov.tasks;

import android.os.Bundle;

import com.telly.groundy.GroundyService;
import com.telly.groundy.GroundyTask;
import com.telly.groundy.TaskResult;

import alextimonov.TestApplication;
import alextimonov.api.Api;


public abstract class GroundyTaskBase extends GroundyTask {

    public static final String FAILURE_EXCEPTION = "failure_exception";
    public static final String RESULT_TASK_ID = "result_task_id";

    @Override
    protected TaskResult succeeded() {
        return super.succeeded().add(RESULT_TASK_ID, getId());
    }

    @Override
    protected TaskResult failed() {
        return super.failed().add(RESULT_TASK_ID, getId());
    }

    protected TaskResult failed(Exception error) {
        return failed().add(FAILURE_EXCEPTION, error);
    }


    protected Api getApi() {
        return new Api(getApplication());
    }

    private TestApplication getApplication() {
        return ((TestApplication) ((GroundyService) getContext()).getApplication());
    }

    @Override
    protected TaskResult cancelled() {
        return super.cancelled().add(RESULT_TASK_ID, getId());
    }

    @Override
    public void updateProgress(int progress, Bundle extraData) {
        if (extraData == null)
            extraData = new Bundle();

        extraData.putLong(RESULT_TASK_ID, getId());
        super.updateProgress(progress, extraData);
    }

}
