package alextimonov.tasks;

import com.telly.groundy.Groundy;
import com.telly.groundy.TaskResult;

import alextimonov.api.entities.SignUpRequest;
import alextimonov.api.entities.SignUpResponse;
import alextimonov.api.exceptions.ApiException;


public class SignUpTask extends GroundyTaskBase {

    private static final String REQUEST = "request";
    public static final String RESPONSE = "response";

    @Override
    protected TaskResult doInBackground() {
        try {
            SignUpResponse signUpResponse = getApi().signUp((SignUpRequest) getArgs().getSerializable(REQUEST));
            if (!signUpResponse.status) {
                throw new ApiException(signUpResponse.message);
            }
            return succeeded().add(RESPONSE, signUpResponse);
        } catch (ApiException e) {
            return failed(e);
        }
    }

    public static Groundy create(SignUpRequest signUpRequest) {
        return Groundy.create(SignUpTask.class)
                .arg(REQUEST, signUpRequest);
    }
}
