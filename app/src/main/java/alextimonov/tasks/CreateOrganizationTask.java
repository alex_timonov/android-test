package alextimonov.tasks;

import com.telly.groundy.Groundy;
import com.telly.groundy.TaskResult;

import alextimonov.api.entities.CreateOrganizationRequest;
import alextimonov.api.entities.CreateOrganizationResponse;
import alextimonov.api.exceptions.ApiException;


public class CreateOrganizationTask extends GroundyTaskBase {


    public static final String RESPONSE = "response";
    public static final String REQUEST = "request";

    @Override
    protected TaskResult doInBackground() {
        try {
            CreateOrganizationResponse createOrganizationResponse = getApi().createOrganization((CreateOrganizationRequest) getArgs().getSerializable(REQUEST));
            if (!createOrganizationResponse.message.equals("Accepted")) {
                return failed(new ApiException(createOrganizationResponse.message));
            } else {
                return succeeded().add(RESPONSE, createOrganizationResponse);
            }
        } catch (ApiException e) {
            return failed(e);
        }
    }

    public static Groundy create(CreateOrganizationRequest createOrganizationRequest) {
        return Groundy.create(CreateOrganizationTask.class)
                .arg(REQUEST, createOrganizationRequest);
    }
}
