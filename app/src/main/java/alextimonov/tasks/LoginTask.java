package alextimonov.tasks;

import com.telly.groundy.Groundy;
import com.telly.groundy.TaskResult;

import alextimonov.api.entities.LoginResponse;
import alextimonov.api.entities.SignUpRequest;
import alextimonov.api.exceptions.ApiException;


public class LoginTask extends GroundyTaskBase {


    public static final String RESPONSE = "response";
    public static final String REQUEST = "request";

    @Override
    protected TaskResult doInBackground() {
        try {
            LoginResponse loginResponseData = getApi().logIn((SignUpRequest) getArgs().getSerializable(REQUEST));
            if (!loginResponseData.status) {
                return failed(new ApiException(loginResponseData.message));
            } else {
                return succeeded().add(RESPONSE, loginResponseData);
            }
        } catch (ApiException e) {
            return failed(e);
        }
    }

    public static Groundy create(SignUpRequest signUpRequest) {
        return Groundy.create(LoginTask.class)
                .arg(REQUEST, signUpRequest);
    }
}
