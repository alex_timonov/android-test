package alextimonov.activities;

import android.widget.ListView;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import alextimonov.ImagesAdapter;
import alextimonov.R;
import alextimonov.api.entities.ImageResult;

@EActivity(R.layout.activity_images)
public class ImagesActivity extends BaseActivity {

    @ViewById
    ListView list;

    @Extra
    ArrayList<ImageResult> images;

    @Override
    protected void onStart() {
        super.onStart();
        list.setAdapter(new ImagesAdapter(this, images));
    }
}
