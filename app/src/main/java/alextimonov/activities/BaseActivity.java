package alextimonov.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.telly.groundy.CallbacksManager;
import com.telly.groundy.Groundy;

import alextimonov.TestApplication;
import alextimonov.properties.UserPreferences_;

public class BaseActivity extends AppCompatActivity {

    protected CallbacksManager callbacksManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbacksManager = CallbacksManager.init(savedInstanceState);
        callbacksManager.linkCallbacks(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        callbacksManager.onSaveInstanceState(outState);
    }

    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }


    protected UserPreferences_ getPreferences() {
        return ((TestApplication) getApplication()).getPreferences();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callbacksManager.onDestroy();
    }

    public void queueGroundyTask(Groundy groundy, Object callback) {
        groundy.callback(callback).callbackManager(callbacksManager).queueUsing(getApplicationContext());
    }
}
