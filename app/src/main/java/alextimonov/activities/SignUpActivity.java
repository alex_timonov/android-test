package alextimonov.activities;

import android.widget.EditText;

import com.telly.groundy.annotations.OnFailure;
import com.telly.groundy.annotations.OnSuccess;
import com.telly.groundy.annotations.Param;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import alextimonov.R;
import alextimonov.StringHelpers;
import alextimonov.api.entities.SignUpRequest;
import alextimonov.api.entities.SignUpResponse;
import alextimonov.api.exceptions.ApiException;
import alextimonov.tasks.GroundyTaskBase;
import alextimonov.tasks.SignUpTask;

@EActivity(R.layout.activity_signup)
public class SignUpActivity extends BaseActivity {
    @ViewById
    EditText username;
    @ViewById
    EditText password;
    @ViewById
    EditText email;

    @Click(R.id.signUp)
    void signUp() {
        try {
            StringHelpers.isNullOrEmptyString(username.getText().toString(), getString(R.string.username_is_empty));
            StringHelpers.isNullOrEmptyString(password.getText().toString(), getString(R.string.password_is_empty));
            StringHelpers.checkEmail(email.getText().toString(), getString(R.string.email_is_invalid));
            if (password.getText().toString().length() < 6) {
                throw new IllegalArgumentException(getString(R.string.password_should_be_at_least));
            }
        } catch (IllegalArgumentException e) {
            showToast(e.getMessage());
        }
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.username = username.getText().toString();
        signUpRequest.password = password.getText().toString();
        signUpRequest.roleId = 2;
        signUpRequest.email = email.getText().toString();
        signUpRequest.organizationId = 1;
        queueGroundyTask(SignUpTask.create(signUpRequest), this);
    }

    @OnSuccess(SignUpTask.class)
    @OnFailure(SignUpTask.class)
    public void signupCompleted(@Param(GroundyTaskBase.FAILURE_EXCEPTION) ApiException exception, @Param(SignUpTask.RESPONSE) SignUpResponse signUpResponse) {
        if (exception != null) {
            showToast(exception.getMessage());
        } else {
            showToast(getString(R.string.user_created));
            finish();
        }
    }

}
