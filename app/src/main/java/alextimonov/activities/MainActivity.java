package alextimonov.activities;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

import alextimonov.R;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @Click(R.id.signUp)
    void signUp() {
        SignUpActivity_.intent(this).start();
    }

    @Click(R.id.logIn)
    void login() {
        LogInActivity_.intent(this).start();
    }

}
