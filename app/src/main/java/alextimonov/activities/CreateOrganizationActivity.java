package alextimonov.activities;

import android.widget.EditText;

import com.telly.groundy.annotations.OnFailure;
import com.telly.groundy.annotations.OnSuccess;
import com.telly.groundy.annotations.Param;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import alextimonov.R;
import alextimonov.StringHelpers;
import alextimonov.api.entities.CreateOrganizationRequest;
import alextimonov.api.entities.CreateOrganizationResponse;
import alextimonov.api.entities.ImagesResponse;
import alextimonov.api.exceptions.ApiException;
import alextimonov.tasks.CreateOrganizationTask;
import alextimonov.tasks.GetImagesTask;
import alextimonov.tasks.GroundyTaskBase;

@EActivity(R.layout.activity_create_organization)
public class CreateOrganizationActivity extends BaseActivity {

    @ViewById
    EditText name;
    @ViewById
    EditText email;


    @Click(R.id.createOrganization)
    void createOrganization() {
        try {
            StringHelpers.isNullOrEmptyString(name.getText().toString(), getString(R.string.name_is_empty));
            StringHelpers.isNullOrEmptyString(email.getText().toString(), getString(R.string.email_is_invalid));
        } catch (IllegalArgumentException e) {
            showToast(e.getMessage());
        }
        CreateOrganizationRequest createOrganizationRequest = new CreateOrganizationRequest();
        createOrganizationRequest.name = name.getText().toString();
        createOrganizationRequest.email = email.getText().toString();
        queueGroundyTask(CreateOrganizationTask.create(createOrganizationRequest), this);
    }

    @Click(R.id.loadImages)
    void loadImages() {
        queueGroundyTask(GetImagesTask.create(), this);
    }

    @OnSuccess(CreateOrganizationTask.class)
    @OnFailure(CreateOrganizationTask.class)
    public void loginCompleted(@Param(GroundyTaskBase.FAILURE_EXCEPTION) ApiException exception,
                               @Param(CreateOrganizationTask.RESPONSE) CreateOrganizationResponse createOrganizationResponse) {
        if (exception != null) {
            showToast(exception.getMessage());
        } else {
            showToast(getString(R.string.organization_created));
        }
    }

    @OnSuccess(GetImagesTask.class)
    @OnFailure(GetImagesTask.class)
    public void loadImages(@Param(GroundyTaskBase.FAILURE_EXCEPTION) ApiException exception,
                           @Param(GetImagesTask.RESPONSE) ImagesResponse imagesResponse) {
        if (exception != null) {
            showToast(exception.getMessage());
        } else {
            ImagesActivity_.intent(this).images(imagesResponse.data.results).start();
        }
    }
}
