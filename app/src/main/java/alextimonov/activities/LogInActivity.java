package alextimonov.activities;

import android.widget.EditText;

import com.telly.groundy.annotations.OnFailure;
import com.telly.groundy.annotations.OnSuccess;
import com.telly.groundy.annotations.Param;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import alextimonov.R;
import alextimonov.StringHelpers;
import alextimonov.api.entities.LoginResponse;
import alextimonov.api.entities.SignUpRequest;
import alextimonov.api.exceptions.ApiException;
import alextimonov.tasks.GroundyTaskBase;
import alextimonov.tasks.LoginTask;

@EActivity(R.layout.activity_login)
public class LogInActivity extends BaseActivity {

    @ViewById
    EditText username;
    @ViewById
    EditText password;

    @Click(R.id.logIn)
    void login() {
        try {
            StringHelpers.isNullOrEmptyString(username.getText().toString(), getString(R.string.username_is_empty));
            StringHelpers.isNullOrEmptyString(password.getText().toString(), getString(R.string.password_is_empty));
            if (password.getText().toString().length() < 6) {
                throw new IllegalArgumentException(getString(R.string.password_should_be_at_least));
            }
        } catch (IllegalArgumentException e) {
            showToast(e.getMessage());
        }
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.username = username.getText().toString();
        signUpRequest.password = password.getText().toString();
        queueGroundyTask(LoginTask.create(signUpRequest), this);
    }

    @OnSuccess(LoginTask.class)
    @OnFailure(LoginTask.class)
    public void loginCompleted(@Param(GroundyTaskBase.FAILURE_EXCEPTION) ApiException exception, @Param(LoginTask.RESPONSE) LoginResponse loginResponse) {
        if (exception != null) {
            showToast(exception.getMessage());
        } else {
            CreateOrganizationActivity_.intent(this).start();
        }
    }
}
