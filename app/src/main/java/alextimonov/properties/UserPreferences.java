package alextimonov.properties;

import org.androidannotations.annotations.sharedpreferences.SharedPref;


@SharedPref(SharedPref.Scope.APPLICATION_DEFAULT)
public interface UserPreferences {

    String token();

    String username();

    String password();
}
