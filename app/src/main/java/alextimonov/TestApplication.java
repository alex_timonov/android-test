package alextimonov;

import android.app.Application;

import org.androidannotations.annotations.EApplication;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.net.CookieHandler;

import alextimonov.properties.UserPreferences_;


@EApplication
public class TestApplication extends Application {

    @Pref
    UserPreferences_ preferences;

    public UserPreferences_ getPreferences() {
        return preferences;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MyCookieManager myCookieManager = new MyCookieManager(preferences);
        CookieHandler.setDefault(myCookieManager);
    }
}
