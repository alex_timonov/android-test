package alextimonov;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import alextimonov.api.entities.ImageResult;

public class ImagesAdapter extends BaseAdapter {
    private ArrayList<ImageResult> results;
    private Context context;

    static class ViewHolder {
        public ImageView image;
    }

    public ImagesAdapter(Context context, ArrayList<ImageResult> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int position) {
        return results.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_image, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        Picasso.with(convertView.getContext()).load(results.get(position).MediaUrl).into(holder.image);

        return convertView;
    }
} 